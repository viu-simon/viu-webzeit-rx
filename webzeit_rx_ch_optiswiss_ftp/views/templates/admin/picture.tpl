
<div id="fieldset_1" class="panel">
	<div class="panel-heading">Bild</div>
		<div class="form-wrapper">
			<a href="{$rxbo->picture}" class="fancybox"><img src="{$rxbo->picture}" style="height: 200px;"/></a>
		</div>
</div>

{literal}

	<script>
		$('#tabRxOrder a').click(function (e) {
			e.preventDefault()
			$(this).tab('show')
		});
		
		$(".rx-img-wrapper a").fancybox({
			'transitionIn'	:	'elastic',
			'transitionOut'	:	'elastic',
			'speedIn'		:	600, 
			'speedOut'		:	200, 
			'overlayShow'	:	false
		});
	
	</script>
	
{/literal}