
$(document).ready(function() {
	
	$('.rx-file:file').change(function(){
	    var file = this.files[0];
	    var name = file.name;
	    var size = file.size;
	    var type = file.type;
	    //Your validation
	});
	
	$('.rx-upload:button').click(function(){
    	var formData = new FormData($('form')[0]);
	    $.ajax({
	        url: '/rxupload',
	        type: 'POST',
	        xhr: function() {  // Custom XMLHttpRequest
	            var myXhr = $.ajaxSettings.xhr();
	            if(myXhr.upload){ // Check if upload property exists
	                myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
	            }
	            return myXhr;
	        },
	        //Ajax events
	       // beforeSend: beforeSendHandler,
	        success: function(data) {
		        var data = $.parseJSON(data);
		       
		       
		        
		        $('select#rx_select').prepend($('<option>', {
				    value: data.id,
				    text: 'Dein hochgeladenes Foto'
				})).val(data.id);
				
				$.uniform.update("#rx_select");
				
				$.uniform.update($('#rxOptionUpload').attr('checked',false));
				$.uniform.update($('#rxOptionExisting').attr('checked',true));
				
				$('#rx_uploaded img').attr('src',data.file);
				$('#rx_uploaded').show();
				
				$('.webzeit-rx-existing').show();
				
				 $('#helperDialogUpload').modal('hide');
				
				 
		    },
	        //error: errorHandler,
	        // Form data
	        data: formData,
	        //Options to tell jQuery not to process data or worry about content-type.
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	});
	
	function progressHandlingFunction(e){
	    if(e.lengthComputable){
	        $('progress').attr({value:e.loaded,max:e.total});
	    }
	}

    // enable uniform on mobile devices (Patch by simon)
    if (isMobile){
        $("select.form-control,input[type='checkbox']:not(.comparator), input[type='radio'],input#id_carrier2, input[type='file']").uniform();
    }
});