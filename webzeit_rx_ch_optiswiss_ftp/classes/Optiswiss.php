<?php

/**
 * Created by PhpStorm.
 * User: simon
 * Date: 16.01.17
 * Time: 11:32
 */
class Optiswiss
{
    private $ftp_user;
    private $ftp_url;
    private $ftp_pw;

    public function __construct()
    {
        $this->ftp_url = 'ftp.optiswiss.ch';
        $this->ftp_user = 'viu';
        $this->ftp_pw = '4o33I8$2';
    }

    public function sendOrder($id_order, $id_rx)
    {

        if ($id_rx > 0)
            $res = $this->ftprequest($id_order, $id_rx);

        if (strlen($res->url) > 3) {

            $this->smarty->assign(array(
                'opti_sso' => array($id_rx => $res->url)
            ));

            // change order status
            $history = new OrderHistory();
            $history->id_order = (int)$id_order;
            $history->id_employee = (int)$this->context->employee->id;
            $history->id_order_state = 4;
            $history->changeIdOrderState(4, $id_order);
            $history->add();
        }
    }

    private function ftprequest($id_order, $id_rx)
    {

        $order = new Order($id_order);
        $customer = new Customer($order->id_customer);
        $rx = webzeitVIUrx::getRXOrderDetailsByRX($id_order, $id_rx);

        $xml_order = $this->createB2BxmlOrder($order, $customer, $rx);

        $tmpfile = $this->makeTmpFile($xml_order, $id_order);

        $remote_file = "viu_$id_order.xml";

        // set up basic connection
        $conn_id = ftp_connect($this->ftp_url);

        // login with username and password
        $login_result = ftp_login($conn_id, $this->ftp_user, $this->ftp_pw);

        // upload a file
        if (ftp_put($conn_id, $remote_file, $tmpfile, FTP_ASCII)) {
            return true;
        } else {
            echo "There was a problem while uploading $tmpfile\n";
        }

// close the connection
        ftp_close($conn_id);
        die();
        /*
                Db::getInstance()->executeS('INSERT INTO ps_optiswiss_log (id_customer,id_order,id_rx,id_employee,date,response,product,id_order_detail, glass, frame)
                                                VALUES('.$order->id_customer.','.$id_order.','.$id_rx.','.$this->context->employee->id.',"'.date('Y-m-d H:i:s').'","'.$response->url.'","'.$rx['product_name'].'","'.$rx['id_order_detail'].'","'.Tools::getValue('oswiss_glas').'","'.Tools::getValue('oswiss_frame').'")');
        */
        return $response;

    }

    private function makeTmpFile($string, $id_order)
    {
        $filename = date("Y-m-d_") . md5(date("d.m.Y H:i:s:u"));
        $path = __DIR__ . "/../tmp/$filename.xml";
        $tmpfile = fopen($path, 'w+');
        fputs($tmpfile, $string);
        fclose($tmpfile);
        return $path;
    }

    private function createB2BxmlOrder($order, $customer, $rx)
    {

        /*
        $products = $order->getProducts();
        foreach($products as $product)
            $model .= Db::getInstance()->getValue('SELECT name FROM ps_product_lang WHERE id_lang = 1 AND id_product ='.$product['product_id']);
        $model = trim($model, ' / ');
        */

        $modelparts = explode(" - ", Tools::getValue('optiswiss_frame_name'));
        $model = $modelparts[0];

        $presets = array('cid' => '403318',
            'glas' => Tools::getValue('oswiss_glas'),
            'frame' => Tools::getValue('oswiss_frame')
        );

        $default_height = Db::getInstance()->getRow('SELECT * FROM ps_optiswiss_frames WHERE code ="' . Tools::getValue('oswiss_frame') . '"');
        if ($rx['view_left'] == 0 || $rx['view_left'] == "0.00" || empty($rx['view_left']))
            $rx['view_left'] = $default_height['height'];
        if ($rx['view_right'] == 0 || $rx['view_right'] == "0.00" || empty($rx['view_right']))
            $rx['view_right'] = $default_height['height'];


        $xml = '
			<b2bOptic
			    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://schema.b2boptic.com/b2boptic_v1.6.0.xsd">
			    <header msgType="ORDER">
			        <customersOrderId>' . $presets['cid'] . '</customersOrderId>
			        <distributorsOrderId>0</distributorsOrderId>
			        <timeStamps>
			            <dateTime step="CREATE">' . date('c') . '</dateTime>
			        </timeStamps>
			        <orderParties role="ORIGINATOR">
			            <id>' . $presets['coid'] . '</id>
			            <name>VIU VENTURES AG</name>
			            <address>
			                <addressLine>Weststrasse 70</addressLine>
			                <city>Zürich</city>
			                <zip>8003</zip>
			                <countryCode>CH</countryCode>
			            </address>
			        </orderParties>
					<software typeOf="SENDER">
			            <name>ShopBackOffice</name>
			            <version>1.5</version>
			        </software>
			        <productCatalog>
			            <name>sf6</name>
			            <release>0</release>
			        </productCatalog>
			        <portalOrderId>0</portalOrderId>
			    </header>
				<items>
			      <item>
			            <parties role="SHIPTO">
			                <id>' . $order->id_customer . '</id>
			            </parties>
			            <referenceNo>' . $order->id . '</referenceNo>
			            <referenceText>' . $order->id . '_' . substr($order->reference, 0, 3) . '_' . $customer->lastname . '_' . $model . '</referenceText>
			            <manufacturer></manufacturer>
			            <pair>
			                <patient>
			                    <id>' . $order->id_customer . '</id>
			                    <name>' . $customer->firstname . ' ' . $customer->lastname . '</name>
			                    <contact>
			                        <firstName>' . $customer->firstname . '</firstName>
			                        <lastName>' . $customer->lastname . '</lastName>
			                    </contact>
			                    <interpupillaryDistanceRight>' . $rx['pd_right'] . '</interpupillaryDistanceRight>
			                    <interpupillaryDistanceLeft>' . $rx['pd_left'] . '</interpupillaryDistanceLeft>
			                     
			                </patient>			                <lens quantity="1" side="RIGHT">
			                    <commercialCode>' . $presets['glas'] . '</commercialCode>
			                    <rxData>
			                        <sphere>' . $rx['r_eye_spherical'] . '</sphere>
			                        <cylinder>
			                            <power>' . $rx['r_eye_cylindrical'] . '</power>
			                            <axis>' . $rx['r_eye_axis'] . '</axis>
			                        </cylinder>
			                        <addition>' . $rx['addition_right'] . '</addition>
			                    </rxData>

			                    <centration>
			                        <monocularCentrationDistance reference="FAR">' . $rx['pd_right'] . '</monocularCentrationDistance>
			                        <height reference="FAR" referenceHeight="OVERBOX">' . $rx['view_right'] . '</height>
			                         
			                    </centration>
			                    <geometry>
			                        <diameter>
			                            <physical>65</physical>
			                            <optical>70</optical>
			                        </diameter>
			                        <thickness reference="DRILLHOLE">1.50</thickness>
			                        <thicknessReduction reference="REDUCEWITHSHAPE">true</thicknessReduction>
			                    </geometry>
			                </lens>
			                <lens quantity="1" side="LEFT">
			                    <commercialCode>' . $presets['glas'] . '</commercialCode>
			                   <rxData>
			                        <sphere>' . $rx['l_eye_spherical'] . '</sphere>
			                        <cylinder>
			                            <power>' . $rx['l_eye_cylindrical'] . '</power>
			                            <axis>' . $rx['l_eye_axis'] . '</axis>
			                        </cylinder>
			                        <addition>' . $rx['addition_left'] . '</addition>
			                    </rxData>
			                    <centration>
			                        <monocularCentrationDistance reference="FAR">' . $rx['pd_left'] . '</monocularCentrationDistance>
			                        <height reference="FAR" referenceHeight="OVERBOX">' . $rx['view_left'] . '</height>
			                     
			                    </centration>
			                    <geometry>
			                        <diameter>
			                            <physical>65</physical>
			                            <optical>70</optical>
			                        </diameter>
			                        <thickness reference="DRILLHOLE">1.50</thickness>
			                        <thicknessReduction reference="REDUCEWITHSHAPE">true</thicknessReduction>
			                    </geometry>
			                </lens>
			                <frame quantity="1">
								<material>PLASTIC</material>
								<commercialCode>' . $presets['frame'] . '</commercialCode>
							</frame>
                            <edging edgingType="ONSHAPE"></edging>
			            </pair>
			            <options>
			                <insurance>true</insurance>
			            </options>
			        </item> 
			    </items>
			</b2bOptic>';

        $doc = new DOMDocument();
        $doc->loadXML($xml);
        $xmlstring = $doc->saveXML();

        return $xmlstring;

    }
}