<?php
	
include_once(dirname(__FILE__).'/../../classes/webzeitVIUrx.php');	
	
class webzeitRXadmintabController extends ModuleAdminController
{
	
	public function __construct()
	{
		
		
		$this->bootstrap = true;
		$this->context = Context::getContext();
		$this->setConfig();
		
		parent::__construct();
		
	}
	

	private function setConfig() {
	
		$this->table = 'webzeit_rx';
    	$this->className = 'webzeitVIUrx';
		$this->identifier = 'id_rx';
		$this->lang = false;
	
	
					
		$this->_select = 'a.*, c.email as email, c.firstname as firstname, c.lastname as lastname ';
		$this->_join = '
			LEFT JOIN `'._DB_PREFIX_.'customer` c ON (c.`id_customer` = a.`id_customer`)
		';
		$this->_orderBy = 'id_rx';
		$this->_orderWay = 'DESC';	
			



	
	   	$this->fields_list = array(
                    'id_rx' => array(
                            'title' => $this->l('ID'),
                            'align' => 'center',
                            'width' => 25
                    ),
     
                     'id_customer' => array(
                            'title' => $this->l('Kunde'),
                            'width' => 'auto',
                            'filter_key' => 'c!id_customer',
                    ),
                    
                    'firstname' => array(
                            'title' => $this->l('Vorname'),
                            'width' => 'auto',
                            'filter_key' => 'c!firstname',
                    ),
                    'lastname' => array(
                            'title' => $this->l('Name'),
                            'width' => 'auto',
                            'filter_key' => 'c!lastname',
                    ),
                     'email' => array(
                            'title' => $this->l('email'),
                            'width' => 'auto',
                            'filter_key' => 'c!email',
                    ),
					'date_add' => array(
                            'title' => $this->l('Datum'),
                            'width' => 'auto'
                    ),
           
           
               );
	
	}
	
	public function renderList() {
		
		$this->addRowAction('edit');
		//$this->addRowAction('delete');
		
		return parent::renderList();
		
	}
	
	

	
	public function renderForm()
	{
	
			$this->fields_form = array(
			'legend' => array(
				'title' => $this->l('Brillenrezept:'),
				'image' => '../img/admin/note.png'
			),
	
			'submit' => array(
				'title' => $this->l('Save'),
			),
			'input' => array(
					
					array(
						'type' => 'hidden',
						'name' => 'id_rx',
						'lang' => false,
						'size' => 33,
						'required' => true,
					),
										
					array(
						'type' => 'text',
						'label' => $this->l('Rechtes Auge: Spähre'),
						'name' => 'r_eye_spherical',
						'lang' => false,
						'size' => 33,
						'required' => false,
					),
					
					array(
						'type' => 'text',
						'label' => $this->l('Rechtes Auge: Achse'),
						'name' => 'r_eye_axis',
						'lang' => false,
						'size' => 33,
						'required' => false,
					),
					
					array(
						'type' => 'text',
						'label' => $this->l('Rechtes Auge: Zylinder'),
						'name' => 'r_eye_cylindrical',
						'lang' => false,
						'size' => 33,
						'required' => false,
					),
					
				
					
					array(
						'type' => 'text',
						'label' => $this->l('Rechtes Auge: Addition'),
						'name' => 'addition_right',
						'lang' => false,
						'size' => 33,
						'required' => false,
					),
					
					array(
						'type' => 'text',
						'label' => $this->l('Rechtes Auge: PD'),
						'name' => 'pd_right',
						'lang' => false,
						'size' => 33,
						'required' => false,
					),
					
					
					array(
						'type' => 'text',
						'label' => $this->l('Rechtes Auge: Sichthöhe'),
						'name' => 'view_right',
						'lang' => false,
						'size' => 33,
						'required' => false,
					),
					
					
					array(
						'type' => 'text',
						'label' => $this->l('Linkes Auge: Spähre'),
						'name' => 'l_eye_spherical',
						'lang' => false,
						'size' => 33,
						'required' => false,
					),
					
					array(
						'type' => 'text',
						'label' => $this->l('Linkes Auge: Achse'),
						'name' => 'l_eye_axis',
						'lang' => false,
						'size' => 33,
						'required' => false,
					),
					
					array(
						'type' => 'text',
						'label' => $this->l('Linkes Auge: Zylinder'),
						'name' => 'l_eye_cylindrical',
						'lang' => false,
						'size' => 33,
						'required' => false,
					),
					
					array(
						'type' => 'text',
						'label' => $this->l('Linkes Auge: Addition'),
						'name' => 'addition_left',
						'lang' => false,
						'size' => 33,
						'required' => false,
					),
					
					array(
						'type' => 'text',
						'label' => $this->l('Linkes Auge: PD'),
						'name' => 'pd_left',
						'lang' => false,
						'size' => 33,
						'required' => false,
					),
					
					
					array(
						'type' => 'text',
						'label' => $this->l('Linkes Auge: Sichthöhe'),
						'name' => 'view_left',
						'lang' => false,
						'size' => 33,
						'required' => false,
					),
					
					
					array(
						'type' => 'text',
						'label' => $this->l('Total PD'),
						'name' => 'pd_total',
						'lang' => false,
						'size' => 33,
						'required' => false,
					),
						
						
					
				
										
					
						
														
				)
			);

			$rx = new webzeitVIUrx(Tools::getValue('id_rx'));
			$this->context->smarty->assign(array('rxbo' => $rx));
			$this->content .= parent::renderForm(); 
			$this->content .= $this->createTemplate('../../../../modules/webzeit_rx/views/templates/admin/picture.tpl')->fetch(); 
		
	}

	 public function initToolbar()
    {

	}
	
}