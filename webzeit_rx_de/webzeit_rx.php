<?php

/*
 * webzeit_rx
 * by webzeit
 * Copyright (C) 2015 webzeit gmbh
 *
 */


if (!defined('_PS_VERSION_'))
    exit;

include_once(dirname(__FILE__).'/classes/webzeitVIUrx.php');

class webzeit_rx extends Module
{

    public function __construct()
    {
        $this->name = 'webzeit_rx';
        $this->tab = 'back_office_features';
        $this->version = '0.2';
        $this->author = 'webzeit gmbh';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.6');

        parent::__construct();

        $this->displayName = $this->l('webzeit_rx');
        $this->description = $this->l('VIU Brillenrezept');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }


    // INSTALL / UNINSTALL

    public function install()
    {
        if (!parent::install())
            return false;
        return true;
    }
    public function uninstall()
    {
        if (!parent::uninstall())
            return false;
        return true;
    }

    public function hookActionValidateOrder($params) {
        // validate Order
        return webzeitVIUrx::setOrderId($params['cart']->id,$params['customer']->id,$params['order']->id);
    }

    public function hookAdminOrder($params) {

        include_once(_PS_MODULE_DIR_.'/webzeit_ordertasks/classes/webzeitOrderTasksLog.php');
        $order = new Order($params['id_order']);
        if($order->module == 'webzeit_tryathome')
            return '';

        if(Tools::isSubmit('edit_rx_bo')) {
            $this->saveRXBO();
            webzeitOrderTasksLog::addLog($order->id, "RX", "Brillenrezept gespeichert");
        }

        if(Tools::getValue('rx_change_bo') == 'reassign') {
            $this->saveChangeRXBO();
            webzeitOrderTasksLog::addLog($order->id, "RX", "Neues Brillenrezept übernommen");

        }

        if(Tools::isSubmit('requestOptovision')) {
            $this->sendOptoVision($params['id_order']);
            webzeitOrderTasksLog::addLog($order->id, "OSV", "Brillenrezept zu Optovision hochgeladen");
        }elseif(Tools::isSubmit('requestOptiSwiss')) {
            $this->sendOptiSwiss($params['id_order']);
            webzeitOrderTasksLog::addLog($order->id, "OSS", "Brillenrezept zu OptiSwiss hochgeladen");
        }

        if(Tools::isSubmit('bo_viu_quittung'))
            $this->sendVersicherungsQuittung($params);




        $ossGlasses = Db::getInstance()->executeS('SELECT * FROM ps_optiswiss_glasses ORDER BY position ASC ');
        foreach($ossGlasses as $k => $v)
            $selGlas .= '<option value="'.$v['code'].'">'.$v['name'].' ('.$v['description'].')</option>';
        $selGlas = '<select name="oswiss_glas">'.$selGlas.'</select>';


        $ossFrames = Db::getInstance()->executeS('SELECT * FROM ps_optiswiss_frames');
        foreach($ossFrames as $k => $v)
            $selFrame .= '<option value="'.$v['code'].'">'.$v['product'].'</option>';
        $selFrame = '<select name="oswiss_frame">'.$selFrame.'</select>';

        $ossLog = Db::getInstance()->executeS('SELECT ol.*, e.firstname FROM ps_optiswiss_log ol
												 LEFT JOIN ps_employee e ON (e.id_employee = ol.id_employee) WHERE ol.id_order = "'.$params['id_order'].'" ORDER BY ol.date');
        foreach ($ossLogUngrouped as $log)
            $ossLog[$log['id_order_detail']][] = $log;

        $osvLogUngrouped = Db::getInstance()->executeS('SELECT ol.*, e.firstname FROM ps_optovision_log ol
												 LEFT JOIN ps_employee e ON (e.id_employee = ol.id_employee) WHERE ol.id_order = "'.$params['id_order'].'" ORDER BY ol.date');
        foreach ($osvLogUngrouped as $log)
            $osvLog[$log['id_order_detail']][] = $log;


        $rxCustomer = webzeitVIUrx::getRXByCustomer($order->id_customer);

        $rx = webzeitVIUrx::getRXOrderDetails($params['id_order']);
        foreach($rx as $record) {

            if($record['id_rx'] > 0) {

                $rxClean[$record['id_rx']] = $record;
                $rxClean[$record['id_rx']]['latest_picture'] = $rxCustomer[0]['latest_picture'];
                $rxCleanTabs[$record['id_rx']]['id_order_detail'] = $record['id_order_detail'];
                $rxCleanTabs[$record['id_rx']]['id_rx'] = $record['id_rx'];
                $rxCleanTabs[$record['id_rx']]['product_name'] .= $record['product_name'].'<br/> ';
            }
        }

        $rxCustomer = webzeitVIUrx::getRXByCustomer($order->id_customer);
        $orderProducts = Db::getInstance()->executeS('SELECT * FROM ps_order_detail WHERE id_order ='.$params['id_order'].' AND linkedto = 0');

        $quittung = Db::getInstance()->getRow('SELECT * FROM ps_viu_versicherungsbetrag WHERE id_order = '.$order->id);
        $amount = $order->total_paid;
        if($quittung['id_versicherung'] > 0)
            $amount = $quittung['amount'];

        $this->smarty->assign(array(
            'rxs' => $rxClean,
            'rxsTabs' => $rxCleanTabs,
            'opti_glas' => $selGlas,
            'opti_frame' => $this->getOptiswissData($orderProducts),
            'opti_frame_list' => $selFrame,
            'opti_log' => $ossLog,
            'optov_log' => $osvLog,
            'rxsCustomer' => $rxCustomer,
            'rxidcustomer' => $order->id_customer,
            'orderProducts' => $orderProducts,
            'orderid' => $params['id_order'],
            'rx_amount' => $amount,
            'optovision' => $this->getOptovisionData($orderProducts)
        ));

        return $this->display(__FILE__, 'rx_bo.tpl');
    }

    public function getOptiswissData($orderProducts)
    {
        foreach ($orderProducts as $order) {

            $frame = $this->getOptiswissFrame(Db::getInstance()->getValue('SELECT frame FROM ps_viu_products WHERE id_product = ' . $order['product_id']));

            $selection = null;

            foreach($frame as $k => $v) {
                $selection .= '<option value="' . $v['code'] . '">' . $v['product'] . '</option>';
            }
            $selFrame = '<select name="oswiss_frame">'.$selection.'</select>';

            if($selection != NULL)
                $ret[$order['id_rx']] = $selFrame;
        }

        return $ret;
    }

    public function getOptiswissFrame($frame)
    {
        $sql = "SELECT product, code FROM ps_optiswiss_frames WHERE frame  = '$frame'";
        return Db::getInstance()->ExecuteS($sql);
    }

    public function getOptovisionFrame($frame)
    {
        $sql = "SELECT model, shapeid FROM ps_optovision_frames WHERE frame  = '$frame'";
        return Db::getInstance()->ExecuteS($sql);
    }

    public function getOptovisionData($orderProducts)
    {
        $ret = array();

        $colors = array(
            array('code' => 'VI1', 'desc' => 'RX - Schwarz 40-85%'),
            array('code' => 'VI3', 'desc' => 'RX - Blau 85%'),
            array('code' => 'VI2', 'desc' => 'RX - Braun 40-85%'),
            array('code' => 'VI4', 'desc' => 'RX - Grün 85%'),
            array('code' => 'BLF', 'desc' => 'Blau 12'),
            array('code' => 'BR1', 'desc' => 'Braun 15'),
            array('code' => 'BR2', 'desc' => 'Braun 25'),
            array('code' => 'BR5', 'desc' => 'Braun 50'),
            array('code' => 'BR7', 'desc' => 'Braun 75'),
            array('code' => 'BR8', 'desc' => 'Braun 85'),
            array('code' => 'BRF', 'desc' => 'Braun 12'),
            array('code' => 'GR2', 'desc' => 'Grau 25'),
            array('code' => 'GR5', 'desc' => 'Grau 50'),
            array('code' => 'GR7', 'desc' => 'Grau 75'),
            array('code' => 'GR8', 'desc' => 'Grau 85'),
            array('code' => 'GRF', 'desc' => 'Grau 12'),
            array('code' => 'GU2', 'desc' => 'Grün 25'),
            array('code' => 'GU5', 'desc' => 'Grün 50'),
            array('code' => 'GU7', 'desc' => 'Grün 75'),
            array('code' => 'GU8', 'desc' => 'Grün 85'),
            array('code' => 'V3', 'desc'  => 'Braun 10-60'),
            array('code' => 'V6', 'desc'  => 'Grau 10-60'),
            array('code' => 'SF5', 'desc' => 'Sun&Fun 85 Grün'),
            array('code' => 'SF7', 'desc' => 'Sun&Fun 70 Bernstein'),
            array('code' => 'SF8', 'desc' => 'Sun&Fun 80 Braun'),
            array('code' => 'VI1', 'desc' => 'Black 40-85%'),
            array('code' => 'VI2', 'desc' => 'Brown 40-85%'),
            array('code' => 'VI3', 'desc' => 'Blue 85%'),
            array('code' => 'VI4', 'desc' => 'Green 85%'),
        );

        $dt = array(
            'exclude' => array(58,59,67,69,108,135),
            'model' => array(
                146 => array( // glas mit korrektur
                    149 => array(array('code' => 'L15PNT', 'title' => 'Lager'), array('code' => 'SVR15', 'title' => 'RX', 'type' => 'rx')),
                    31  => array(array('code' => 'L16PNT', 'title' => 'Lager'), array('code' => 'SVR16', 'title' => 'RX', 'type' => 'rx')),
                    61  => array(array('code' => 'L67PNT', 'title' => 'Lager'), array('code' => 'SVR67', 'title' => 'RX', 'type' => 'rx')),
                    65  => array(array('code' => 'SV8ASL', 'title' => 'Lager'), array('code' => 'SVR74', 'title' => 'RX', 'type' => 'rx')),
                ),
                147 => array( // ohne korrektur
                    152 => array(array('code' => 'L15PNT', 'title' => 'Lager')),
                ),
                150 => array( // sonnenbrille mit korrektur
                    149 => array(array('code' => 'SVR15', 'title' => 'SV-RX 1.5', 'type' => 'rx'), array('code' => 'SC5SOH', 'title' => 'Lager')),
                    31  => array(array('code' => 'SVR16', 'title' => 'SV-RX 1.6', 'type' => 'rx'), array('code' => 'SC6SOH', 'title' => 'Lager')),
                    61  => array(array('code' => 'SVR67', 'title' => 'SV-RX 1.67', 'type' => 'rx')),
                ),
            ),
            'antiref' => array( // antireflection needed by rx
                146 => array('code' => 'SEN', 'title' => 'HMC ++'),
                147 => array('code' => 'SEN', 'title' => 'HMC ++'),
                150 => array('code' => 'SHB', 'title' => 'BHMC'),
            )
        );

        foreach ($orderProducts as $order){

            $od = Db::getInstance()->executeS('SELECT id_order_detail, product_id, id_rx FROM ps_order_detail WHERE linkedto = ' . $order['id_order_detail']);

            $model = null;

            foreach ($od as $item) {
                $pid = $item['product_id'];
                $pointer = $dt['model'][$pid];

                if(in_array($item, $dt['exclude']))
                    break;

                if(in_array($pid, array(150))){
                    echo $pid;
                    $ret[$order['id_rx']]['colors'] = $colors;
                }

                // if "gläser mit korrektur", "gläser ohne korrektur", "sonnenbrille mit korrektur"
                if(is_array($pointer) && $model == null){
                    $model = $pointer;

                    // glasstype
                    foreach ($od as $subitem) {
                        $subpid = $subitem['product_id'];

                        if(array_key_exists($subpid, $dt['antiref']))
                            $ret[$order['id_rx']]['antiref'] = $dt['antiref'][$subpid];

                        if (is_array($model[$subpid])){
                            $ret[$order['id_rx']]['glass'] = $model[$subpid];
                            break;
                        }
                    }
                }
            }

            $sql = 'SELECT frame FROM ps_viu_products WHERE id_product = ' . $order['product_id'];
            $frame = $this->getOptovisionFrame(Db::getInstance()->getValue($sql));
            $selection = null;

            foreach($frame as $k => $v) {
                $selection .= '<option value="' . $v['shapeid'] . '">' . $v['model'] . '</option>';
            }
            $selFrame = '<select name="optovision-frame">'.$selection.'</select>';

            if($selection != NULL)
                $ret[$order['id_rx']]['frame'] = $selFrame;

            unset($model);
        }
        return $ret;
    }

    public function sendVersicherungsQuittung($params) {

        $order = new Order((int)$params['id_order']);
        $customer = new Customer($order->id_customer);
        $context = Context::getContext();
        $download = false;

        if(Tools::getValue('bo_viu_quittung') == 'download')
            $download = true;


        if (!Validate::isLoadedObject($order))
            die(Tools::displayError('The order cannot be found within your database.'));

        $frames = Db::getInstance()->getValue('SELECT COUNT(id_order_detail) FROM ps_order_detail WHERE  linkedto = 0 AND id_order = '.$order->id);
        $quittung = Db::getInstance()->getRow('SELECT * FROM ps_viu_versicherungsbetrag WHERE id_order = '.$order->id);

        if($quittung['id_versicherung'] > 0) {
            $amount = $quittung['amount'];
            if(Tools::isSubmit('bo_viu_quittung_amount'))
                $amount = Tools::getValue('bo_viu_quittung_amount');
            Db::getInstance()->Execute('UPDATE ps_viu_versicherungsbetrag SET date_add = "'.date('Y-m-d H:i:s').'", amount = "'.$amount.'" 
										WHERE id_order = '.$order->id);
        } else {
            $amount = Tools::getValue('bo_viu_quittung_amount');
            Db::getInstance()->Execute('INSERT INTO ps_viu_versicherungsbetrag (id_order,date_add,amount) 
										VALUES ('.$order->id.',"'.date('Y-m-d H:i:s').'","'.$amount.'")');
        }

        include_once(_PS_MODULE_DIR_.'/webzeit_rx/classes/webzeitVIUrx.php');
        $context->smarty->assign(array(
            'rxs' => webzeitVIUrx::getRXOrderDetails($order->id),
            'rxs_products' => array(1 => 2),
            'rxs_amount' => $amount,
            'rxs_amountbyframe' => ($amount / $frames),
        ));

        $od = $order->getInvoicesCollection();
        $pdf = new PDF($od, PDF::TEMPLATE_INVOICE,$context->smarty);


        if($download) {
            include_once(_PS_MODULE_DIR_.'/webzeit_ordertasks/classes/webzeitOrderTasksLog.php');
            webzeitOrderTasksLog::addLog($order->id, "RX", "Download Versicherungsquittung");
            $pdf->render(true);
            die();
        } else {

            $file_attachement['content'] = $pdf->render(false);
            $file_attachement['name'] = 'VIU_Quittung_'.$order->reference.'.pdf';
            $file_attachement['mime'] = 'application/pdf';

            $data = array(
                '{firstname}' => $customer->firstname,
                '{lastname}' => $customer->lastname,
                '{email}' => $customer->email,
            );

            Mail::Send(
                (int)$order->id_lang,
                'viu_versicherungsquittung',
                Mail::l('VIU - Versicherungsquittung', (int)$order->id_lang),
                $data,
                $customer->email,
                $customer->firstname.' '.$customer->lastname,
                null,
                null,
                $file_attachement,
                null, _PS_MAIL_DIR_, false, (int)$order->id_shop
            );

            Db::getInstance()->Execute('INSERT INTO ps_viu_versicherungsquittung (id_order,date,email) 
										VALUES ('.$order->id.',"'.date('Y-m-d H:i:s').'","'.$customer->email.'")');

            include_once(_PS_MODULE_DIR_.'/webzeit_ordertasks/classes/webzeitOrderTasksLog.php');
            webzeitOrderTasksLog::addLog($order->id, "RX", "Versicherungsquittung per Email versendet");

        }


    }


    public function saveRXBO() {

        $id_rx = Tools::getValue('edit_rx_bo');
        if($id_rx > 0) {
            $rx = new webzeitVIUrx($id_rx);
            $rx->copyFromPost();
            //if(Tools::getValue('rx_valid') == 2)
            $rx->valid = Tools::getValue('rx_valid');
            $rx->save();
        }

    }

    public function saveChangeRXBO() {

        $id_order_detail = Tools::getValue('rx_change_idorderposition');
        $action = Tools::getValue('rx_change_rxid');
        $id_customer = Tools::getValue('rx_change_customer');

        if(!is_numeric($id_order_detail) || $id_order_detail < 1 || $id_customer < 1)
            return false;

        if($action == 'no') {

            Db::getInstance()->execute("UPDATE ps_order_detail SET id_rx = '0' WHERE id_order_detail = '".$id_order_detail."'");

        } else if ($action == 'new') {

            $rx = new webzeitVIUrx();
            $rx->id_customer = $id_customer;
            $rx->save();

            Db::getInstance()->execute("UPDATE ps_order_detail SET id_rx = '".$rx->id."' WHERE id_order_detail = '".$id_order_detail."'");

        } else if(is_numeric($action) && $action > 0) {

            $newid = webzeitVIUrx::createNewFromOld($action);
            Db::getInstance()->execute("UPDATE ps_order_detail SET id_rx = '".$newid."' WHERE id_order_detail = '".$id_order_detail."'");
        }


    }

    public function sendOptiSwiss($id_order) {

        $id_rx = Tools::getValue('rx_id');
        if($id_rx > 0)
            $res = $this->SOAPrequest($id_order, $id_rx);

        if(strlen($res->url) > 3) {

            $this->smarty->assign(array(
                'opti_sso' => array($id_rx => $res->url)
            ));

            // change order status
            $history = new OrderHistory();
            $history->id_order = (int)$id_order;
            $history->id_employee = (int)$this->context->employee->id;
            $history->id_order_state = 4;
            $history->changeIdOrderState(4, $id_order);
            $history->add();
        }
    }

    public function sendOptoVision($id_order)
    {

        $id_rx = Tools::getValue('rx_id');
        if ($id_rx > 0)
            $res = $this->SOAPrequestOPTO($id_order, $id_rx);

        if ($res != false) {
            $history = new OrderHistory();
            $history->id_order = (int)$id_order;
            $history->id_employee = (int)$this->context->employee->id;
            $history->id_order_state = 4;
            $history->changeIdOrderState(4, $id_order);

            $history->add();

        }
    }

    private function SOAPrequestOPTO($id_order, $id_rx) {

        $order = new Order($id_order);
        $customer = new Customer($order->id_customer);
        $rx = webzeitVIUrx::getRXOrderDetailsByRX($id_order, $id_rx);
        $frame = Tools::getValue('optovision-frame');
        $glass = Tools::getValue('optovision-glass');
        $color = Tools::getValue('optovision-glasscolor');
        $antiref = Tools::getValue('optovision-antiref');

        $xml_order = trim($this->createB2BxmlOrderOPTO($order, $customer, $rx, $frame, $glass, $color, $antiref));

        $requestParams = array(
            'manufacturer' => 'TEST',
            'customerid' => '12291',
            'password' => '357VEZ86',
            'b2bxmlorder' => $xml_order
        );
        try {

            $client = new SoapClient('http://service2.optovision.de/B2BOrder', array('trace' => 1));

            $response = $client->SendB2BOrder($requestParams);
            $xml_response = simplexml_load_string($response->return);

            Db::getInstance()->execute('INSERT INTO ps_optovision_log (id_customer,id_order,id_rx,id_employee,date,response,product,id_order_detail, glass, frame, opto_reference) 
										VALUES('.$order->id_customer.','.$id_order.','.$id_rx.','.$this->context->employee->id.',"'.date('Y-m-d H:i:s').'","'.$xml_response->items->item->orderStatus.'","'.$rx['product_name'].'","'.$rx['id_order_detail'].'","'.$glass.'","'.$frame.'","'.$xml_response->items->item->referenceNo.'")');

            return $response;
        }
        catch(SoapFault $ex){
            die("optovision error: ".$client->__getLastResponse());
            return false;
        }

    }

    private function SOAPrequest($id_order, $id_rx) {

        $order = new Order($id_order);
        $customer = new Customer($order->id_customer);
        $rx = webzeitVIUrx::getRXOrderDetailsByRX($id_order, $id_rx);

        $xml_order = $this->createB2BxmlOrder($order, $customer, $rx);

        $requestParams = array(
            'manufacturer' => 'OSDE',
            //'manufacturer' => 'OSCH',
            //'username' => 'VIUb2boptic_test',
            //'password' => 'opt14tst$$',

            //	'username' => '8421221',
            //	'password' => 'Orlando2015',

            'username' => '9421221',
            'password' => 'Orlando2015',

            'customerid' => 999,
            'orderstatus' => 'OPEN',
            'b2bxmlorder' => $xml_order
        );

        $client = new SoapClient('https://www.osshop.com/b2boptic/b2boptic_webservice_1.0.wsdl', array('trace' => 1));
        $response = $client->UploadOrderToPlatform($requestParams);

        Db::getInstance()->executeS('INSERT INTO ps_optiswiss_log (id_customer,id_order,id_rx,id_employee,date,response,product,id_order_detail, glass, frame) 
										VALUES('.$order->id_customer.','.$id_order.','.$id_rx.','.$this->context->employee->id.',"'.date('Y-m-d H:i:s').'","'.$response->url.'","'.$rx['product_name'].'","'.$rx['id_order_detail'].'","'.Tools::getValue('oswiss_glas').'","'.Tools::getValue('oswiss_frame').'")');

        return $response;

    }

    private function createB2BxmlOrderOPTO($order, $customer, $rx, $frame, $glass, $color, $antiref) {

        $modelparts = explode(" - ", Tools::getValue('optiswiss_frame_name'));
        $model = $modelparts[0];

        $presets = array('cid' => '12290',
            'glas' => $glass,
            'frame' => $frame,
            'commercialcode' => Tools::getValue('optiswiss_frame_name')
        );

        $default_height = Db::getInstance()->getRow('SELECT * FROM ps_optiswiss_frames WHERE code ="'.Tools::getValue('oswiss_frame').'"');
        if($rx['view_left'] == 0 || $rx['view_left'] == "0,00" ||  empty($rx['view_left']))
            $rx['view_left'] = $default_height['height'];
        if($rx['view_right'] == 0 || $rx['view_right'] == "0,00" || empty($rx['view_right']))
            $rx['view_right'] = $default_height['height'];

        if(!$antiref) {
            $antireflection = '';
        }else{
            $antireflection = '<coating coatingType="ANTIREFLEX">
                            <commercialCode>'.$antiref.'</commercialCode>
                         </coating>';
        }

        if(!$color) {
            $xmlcolor = '';
        }else{
            $xmlcolor = '<coating coatingType="COLOR">
                            <commercialCode>'.$color.'</commercialCode>
                         </coating>';
        }

        $xml ='
			<b2bOptic
			    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://schema.b2boptic.com/b2boptic_v1.6.0.xsd">
			    <header msgType="ORDER">
			        <customersOrderId>'.$presets['cid'].'</customersOrderId>
			        <distributorsOrderId>0</distributorsOrderId>
			        <timeStamps>
			            <dateTime step="CREATE">'.date('c').'</dateTime>
			        </timeStamps>
			        <orderParties role="ORIGINATOR">
			            <id>'.$presets['cid'].'</id>
			            <name>VIU VENTURES AG</name>
			            <address>
			                <addressLine>Schillerstraße 41</addressLine>
			                <city>München</city>
			                <zip>80336</zip>
			                <countryCode>DE</countryCode>
			            </address>
			        </orderParties>
					<software typeOf="SENDER">
			            <name>ShopBackOffice</name>
			            <version>1.5</version>
			        </software>
			        <productCatalog>
			            <name>sf6</name>
			            <release>0</release>
			        </productCatalog>
			        <portalOrderId>0</portalOrderId>
			    </header>
				<items>
			      <item>

			            <referenceNo>'.$order->id.'</referenceNo>
			            <referenceText>'.$order->id.'_'.substr($order->reference,0,3).'_'.$customer->lastname.'_'.$model.'</referenceText>
			            <manufacturer></manufacturer>
			            <pair>
			                <patient>
			                    <id>'.$order->id_customer.'</id>
			                    <name>'.$customer->firstname.' '.$customer->lastname.'</name>
			                    <contact>
			                        <firstName>'.$customer->firstname.'</firstName>
			                        <lastName>'.$customer->lastname.'</lastName>
			                    </contact>
			                    <interpupillaryDistanceRight>'.$this->rc($rx['pd_right']).'</interpupillaryDistanceRight>
			                    <interpupillaryDistanceLeft>'.$this->rc($rx['pd_left']).'</interpupillaryDistanceLeft>
			                     
			                </patient>

			                <lens quantity="1" side="RIGHT">
			                    <commercialCode>'.$presets['glas'].'</commercialCode>
			                    <rxData>
			                        '.$this->rxType($rx['addition_type'],$rx['r_eye_spherical'],$rx['addition_right'])['sphere'].'
			                        <cylinder>
			                            <power>'.$this->rc($rx['r_eye_cylindrical']).'</power>
			                            <axis>'.$this->rc($rx['r_eye_axis']).'</axis>
			                        </cylinder>
			                        '.$this->rxType($rx['addition_type'],$rx['r_eye_spherical'],$rx['addition_right'])['addition'].'
			                        '.$this->addElement('prism', ['power' => $rx['prisma1_right'], 'base' => $rx['basis1_right']]).'
			                        '.$this->addElement('prism', ['power' => $rx['prisma2_right'], 'base' => $rx['basis2_right']]).'
			                    </rxData>

			                    <centration>
			                        <monocularCentrationDistance reference="FAR">'.$this->rc($rx['pd_right']).'</monocularCentrationDistance>
			                        <height reference="FAR" referenceHeight="OVERBOX">'.$this->rc($rx['view_right']).'</height>
			                    </centration>
                                '.$antireflection
            .$xmlcolor.'
			                </lens>
			                <lens quantity="1" side="LEFT">
			                    <commercialCode>'.$presets['glas'].'</commercialCode>
			                   <rxData>
			                        '.$this->rxType($rx['addition_type'],$rx['l_eye_spherical'],$rx['addition_left'])['sphere'].'
			                        <cylinder>
			                            <power>'.$this->rc($rx['l_eye_cylindrical']).'</power>
			                            <axis>'.$this->rc($rx['l_eye_axis']).'</axis>
			                        </cylinder>
			                        '.$this->rxType($rx['addition_type'],$rx['l_eye_spherical'],$rx['addition_left'])['addition'].'
			                        '.$this->addElement('prism', ['power' => $rx['prisma1_left'], 'base' => $rx['basis1_left']]).'
			                        '.$this->addElement('prism', ['power' => $rx['prisma2_left'], 'base' => $rx['basis2_left']]).'
			                    </rxData>
			                    <centration>
			                        <monocularCentrationDistance reference="FAR">'.$this->rc($rx['pd_left']).'</monocularCentrationDistance>
			                        <height reference="FAR" referenceHeight="OVERBOX">'.$this->rc($rx['view_left']).'</height>
			                     
			                    </centration>
                                '.$antireflection
            .$xmlcolor.'
			                </lens>
			                <frame quantity="1">
								<material>PLASTIC</material>
								<commercialCode>'.$presets['commercialcode'].'</commercialCode>
								<shape>
									<catalog>
										<shapeId>'.$presets['frame'].'</shapeId>
									</catalog>
								</shape>
							</frame>
							<edging edgingType="ONSHAPE" setRetraceFlagBeforeSending="false">
                                <bevel>
                                    <type>BEVEL</type>
                                    <position posType="AUTO">0</position>
                                </bevel>
                                <polish>false</polish>
                                <drilling>false</drilling>
                            </edging>
			            </pair>
			            <options>
			                <insurance>true</insurance>
			            </options>
			        </item> 
			    </items>
			</b2bOptic>';

        $doc = new DOMDocument();
        $doc->loadXML($xml);
        $xmlstring = $doc->saveXML();
        return $xmlstring;

    }

    /**
     * replace comma by points
     *
     * @param $value
     * @return mixed
     */
    private function rc($value)
    {
        return str_replace(' ','',str_replace(',','.',$value));
    }

    /**
     * recursively add xml elements
     * @param $key
     * @param $value    can be string or array
     * @return string   xml string
     */
    private function addElement($key, $value)
    {
        $add = '';

        if (is_array($value)) {
            foreach ($value as $k => $v) {
                if ($v >= 0)
                    $add .= $this->addElement($k, $v);
            }
        } else {
            $add = $this->rc($value);
        }

        if ($add == '')
            $ret = '';
        else
            $ret = "<$key>$add</$key>";

        return $ret;
    }

    /**
     * summarize spherical and addition if necessary
     * @param $type
     * @param $spherical
     * @param $addition
     * @return array
     */
    private function rxType($type, $sphere, $addition)
    {
        $sphere = $this->rc($sphere);
        $addition = $this->rc($addition);

        switch ($type) {
            case "Lesebrille":
                $sphere += $addition;
                return ['sphere' => "<sphere>$sphere</sphere>", 'addition' => null];
                break;
            case "Fernbrille":
                return ['sphere' => "<sphere>$sphere</sphere>", 'addition' => null];
                break;
            default:
                return ['sphere' => "<sphere>$sphere</sphere>", 'addition' => "<addition>$addition</addition>"];
                break;
        }
    }

    private function createB2BxmlOrder($order, $customer, $rx) {

        $products = $order->getProducts();
        foreach($products as $product)
            $model .= Db::getInstance()->getValue('SELECT name FROM ps_product_lang WHERE id_lang = 1 AND id_product ='.$product['product_id']);
        $model = trim($model, ' / ');

        $presets = array('cid' => '999', 'cid' => '999-1',
            'glas' => Tools::getValue('oswiss_glas'),
            'frame' => Tools::getValue('oswiss_frame')
        );

        $default_height = Db::getInstance()->getRow('SELECT * FROM ps_optiswiss_frames WHERE code ="'.Tools::getValue('oswiss_frame').'"');
        if($rx['view_left'] == 0 || $rx['view_left'] == "0.00" ||  empty($rx['view_left']))
            $rx['view_left'] = $default_height['height'];
        if($rx['view_right'] == 0 || $rx['view_right'] == "0.00" || empty($rx['view_right']))
            $rx['view_right'] = $default_height['height'];


        $xml ='
			<b2bOptic
			    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://schema.b2boptic.com/b2boptic_v1.6.0.xsd">
			    <header msgType="ORDER">
			        <customersOrderId>'.$presets['cid'].'</customersOrderId>
			        <distributorsOrderId>0</distributorsOrderId>
			        <timeStamps>
			            <dateTime step="CREATE">'.date('c').'</dateTime>
			        </timeStamps>
			        <orderParties role="ORIGINATOR">
			            <id>'.$presets['coid'].'</id>
			            <name>VIU VENTURES AG</name>
			            <address>
			                <addressLine>Schillerstraße 41</addressLine>
			                <city>München</city>
			                <zip>80336</zip>
			                <countryCode>DE</countryCode>
			            </address>
			        </orderParties>
					<software typeOf="SENDER">
			            <name>ShopBackOffice</name>
			            <version>1.5</version>
			        </software>
			        <productCatalog>
			            <name>sf6</name>
			            <release>0</release>
			        </productCatalog>
			        <portalOrderId>0</portalOrderId>
			    </header>
				<items>
			      <item>
			            <parties role="SHIPTO">
			                <id>'.$order->id_customer.'</id>
			            </parties>
			            <referenceNo>'.$order->id.'</referenceNo>
			            <referenceText>'.$order->id.'_'.substr($order->reference,0,3).'_'.$customer->lastname.'_'.$model.'</referenceText>
			            <manufacturer></manufacturer>
			            <pair>
			                <patient>
			                    <id>'.$order->id_customer.'</id>
			                    <name>'.$customer->firstname.' '.$customer->lastname.'</name>
			                    <contact>
			                        <firstName>'.$customer->firstname.'</firstName>
			                        <lastName>'.$customer->lastname.'</lastName>
			                    </contact>
			                    <interpupillaryDistanceRight>'.$rx['pd_right'].'</interpupillaryDistanceRight>
			                    <interpupillaryDistanceLeft>'.$rx['pd_left'].'</interpupillaryDistanceLeft>
			                     
			                </patient>			                <lens quantity="1" side="RIGHT">
			                    <commercialCode>'.$presets['glas'].'</commercialCode>
			                    <rxData>
			                        <sphere>'.$rx['r_eye_spherical'].'</sphere>
			                        <cylinder>
			                            <power>'.$rx['r_eye_cylindrical'].'</power>
			                            <axis>'.$rx['r_eye_axis'].'</axis>
			                        </cylinder>
			                        <addition>'.$rx['addition_right'].'</addition>
			                    </rxData>

			                    <centration>
			                        <monocularCentrationDistance reference="FAR">'.$rx['pd_right'].'</monocularCentrationDistance>
			                        <height reference="FAR" referenceHeight="OVERBOX">'.$rx['view_right'].'</height>
			                         
			                    </centration>
			                    <geometry>
			                        <diameter>
			                            <physical>65</physical>
			                            <optical>70</optical>
			                        </diameter>
			                        <thickness reference="DRILLHOLE">1.50</thickness>
			                        <thicknessReduction reference="REDUCEWITHSHAPE">true</thicknessReduction>
			                    </geometry>
			                </lens>
			                <lens quantity="1" side="LEFT">
			                    <commercialCode>'.$presets['glas'].'</commercialCode>
			                   <rxData>
			                        <sphere>'.$rx['l_eye_spherical'].'</sphere>
			                        <cylinder>
			                            <power>'.$rx['l_eye_cylindrical'].'</power>
			                            <axis>'.$rx['l_eye_axis'].'</axis>
			                        </cylinder>
			                        <addition>'.$rx['addition_left'].'</addition>
			                    </rxData>
			                    <centration>
			                        <monocularCentrationDistance reference="FAR">'.$rx['pd_left'].'</monocularCentrationDistance>
			                        <height reference="FAR" referenceHeight="OVERBOX">'.$rx['view_left'].'</height>
			                     
			                    </centration>
			                    <geometry>
			                        <diameter>
			                            <physical>65</physical>
			                            <optical>70</optical>
			                        </diameter>
			                        <thickness reference="DRILLHOLE">1.50</thickness>
			                        <thicknessReduction reference="REDUCEWITHSHAPE">true</thicknessReduction>
			                    </geometry>
			                </lens>
			                <frame quantity="1">
								<material>PLASTIC</material>
								<commercialCode>'.$presets['frame'].'</commercialCode>
							</frame>
			            </pair>
			            <options>
			                <insurance>true</insurance>
			            </options>
			        </item> 
			    </items>
			</b2bOptic>';

        $doc = new DOMDocument();
        $doc->loadXML($xml);
        $xmlstring = $doc->saveXML();
        return $xmlstring;

    }



}

?>