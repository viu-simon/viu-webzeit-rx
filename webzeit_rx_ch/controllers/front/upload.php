<?php


include_once(dirname(__FILE__).'/../../classes/webzeitVIUrx.php');


class webzeit_rxUploadModuleFrontController extends ModuleFrontController
{

	  public $id_customer;
	  public $id_cart;
	  public $id_order;
	  public $upFileName;
	  public $uploadPath;

	  public function initContent() {
	  		
	  	if($_POST['edit_post'] > 0)	{
		 	
		 	$this->setContext();
		 	$res = $this->uploadImage();
			
			die(json_encode($res));
			
			
		} else 
			die('error context!');
		
	
				
	  }  
	  
	  private function setContext() {
		    
		  $this->id_customer = $this->context->cart->id_customer;
		  $this->id_cart = $this->context->cart->id;
		  $this->id_viu_cart = $_POST['edit_post'];
		  $this->upFileName = $this->context->cart->id_customer.'_'.$this->context->cart->id.'_rx_';
		  $this->uploadPathWeb = "/modules/webzeit_rx/uploads/";
		  $this->uploadPath = _PS_MODULE_DIR_."webzeit_rx/uploads/";
	  }
	  
	  private function uploadImage() {
		  
	  
		  $extension = array('.png', '.jpeg', '.gif', '.jpg');
		  $fileAttachment = Tools::fileAttachment('file');
		  if (!empty($fileAttachment['name']) && $fileAttachment['error'] != 0)
				$this->errors[] = Tools::displayError('An error occurred during the file-upload process.');
		 elseif (!empty($fileAttachment['name']) && !in_array(Tools::strtolower(substr($fileAttachment['name'], -4)), $extension) && 
		 			!in_array(Tools::strtolower(substr($fileAttachment['name'], -5)), $extension))
				$this->errors[] = Tools::displayError('Bad file extension');
		 else
		 	return $this->saveImg($fileAttachment);
	
	  }

	  private function saveImg($file) {
		  
		  file_put_contents($this->uploadPath.$this->upFileName.$file['rename'], $file['content']);
		  //webzeitVIUrx::getPK($this->id_cart,$this->id_customer,$this->id_viu_cart)
		  $rx = new webzeitVIUrx(); 
		  $rx->id_cart = $this->id_cart;
		  $rx->id_customer = $this->id_customer;
		  $rx->id_viu_cart = $this->id_viu_cart;
		  $rx->picture = $this->uploadPathWeb.$this->upFileName.$file['rename'];
		  $rx->save();
		  
		  return array('file' => $rx->picture, 'id' => $rx->id);
		  
	  }
  
}