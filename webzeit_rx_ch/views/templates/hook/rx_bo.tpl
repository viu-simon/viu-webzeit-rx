<div class="panel">
    <div class="panel-heading">
        Brillenrezept
    </div>

    <ul id="tabRxOrder" class="nav nav-tabs">
        {foreach from=$rxsTabs item=rx name=rxloop}
            <li class="{if $smarty.foreach.rxloop.first} active{/if}"><a href="#borx{$rx.id_rx}"><span
                            class="badge">{$rx.id_rx}</span><span class="pull-right"
                                                                  style="margin-left: 10px; padding-bottom:10px;font-size: 12px;">{$rx.product_name}</span></a>
            </li>
        {/foreach}
    </ul>
    <div class="tab-content panel">
        {foreach from=$rxs item=rx name=rxloop}
            <div id="borx{$rx.id_rx}" class="tab-pane {if $smarty.foreach.rxloop.first} active{/if}">
                <div class="rx-block">

                    <div class="row">
                        <form method="post"><input type="hidden" name="edit_rx_bo" value="{$rx.id_rx}"/>
                            <div class="col-xs-8">
                                <table id="rx_table" class="table">
                                    <tr>
                                        <td>Rezept-Typ</td>

                                        <td>
                                            <select name="addition_type" id="rx_addition_type">
                                                <option value=""></option>
                                                <option value="Fernbrille"
                                                        {if $rx.addition_type == 'Fernbrille'}selected{/if}>Fernbrille
                                                </option>
                                                <option value="Lesebrille"
                                                        {if $rx.addition_type == 'Lesebrille'}selected{/if}>Lesebrille
                                                </option>
                                                <option value="Gleitsichtbrille"
                                                        {if $rx.addition_type == 'Gleitsichtbrille'}selected{/if}>
                                                    Gleitsichtbrille
                                                </option>
                                                <option value="Arbeitsbrille"
                                                        {if $rx.addition_type == 'Arbeitsbrille'}selected{/if}>
                                                    Arbeitsbrille
                                                </option>
                                            </select>

                                        </td>

                                    </tr>

                                </table>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>

                                <table id="rx_table" class="table">
                                    <thead>
                                    <tr>
                                        <th>
                                            <span class="title_box "></span>
                                        </th>
                                        <th>
                                            <span class="title_box ">Spherical</span>
                                        </th>
                                        <th>
                                            <span class="title_box ">Cylindrical</span>
                                        </th>
                                        <th>
                                            <span class="title_box ">Axis  </span>
                                        </th>
                                        <th>
                                            <span class="title_box ">Add </span>
                                        </th>
                                        <th>
                                            <span class="title_box ">PD&nbsp;&nbsp;&nbsp;</span>
                                        </th>
                                        <th>
                                            <span class="title_box ">Sichthöhe </span>
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td width="100">Rechtes Auge</td>
                                        <td><input type="text" value="{$rx.r_eye_spherical}" name="r_eye_spherical"/>
                                        </td>
                                        <td><input type="text" value="{$rx.r_eye_cylindrical}"
                                                   name="r_eye_cylindrical"/></td>
                                        <td><input type="text" value="{$rx.r_eye_axis}" name="r_eye_axis"/></td>
                                        <td><input type="text" value="{$rx.addition_right}" name="addition_right"/></td>
                                        <td><input type="text" value="{$rx.pd_right}" name="pd_right"/></td>
                                        <td><input type="text" value="{$rx.view_right}" name="view_right"/></td>
                                    </tr>

                                    <tr>
                                        <td width="100">Linkes Auge</td>
                                        <td><input type="text" value="{$rx.l_eye_spherical}" name="l_eye_spherical"/>
                                        </td>
                                        <td><input type="text" value="{$rx.l_eye_cylindrical}"
                                                   name="l_eye_cylindrical"/></td>
                                        <td><input type="text" value="{$rx.l_eye_axis}" name="l_eye_axis"/></td>
                                        <td><input type="text" value="{$rx.addition_left}" name="addition_left"/></td>
                                        <td><input type="text" value="{$rx.pd_left}" name="pd_left"/></td>
                                        <td><input type="text" value="{$rx.view_left}" name="view_left"/></td>
                                    </tr>

                                    </tbody>
                                </table>

                                <table id="rx_table" class="table">
                                    <thead>
                                    <tr>
                                        <th>
                                            <span class="title_box ">&nbsp;</span>
                                        </th>
                                        <th>
                                            <span class="title_box ">Prisma 1</span>
                                        </th>
                                        <th>
                                            <span class="title_box ">Basis 1</span>
                                        </th>
                                        <th>
                                            <span class="title_box ">Prisma 2</span>
                                        </th>
                                        <th>
                                            <span class="title_box ">Basis 2</span>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td width="100">Rechtes Auge</td>
                                        <td><input type="text" value="{$rx.prisma1_right}" name="prisma1_right"
                                             />
                                        </td>
                                        <td><input type="text" value="{$rx.basis1_right}" name="basis1_right"
                                                />
                                        </td>
                                        <td><input type="text" value="{$rx.prisma2_right}" name="prisma2_right"
                                                 />
                                        </td>
                                        <td><input type="text" value="{$rx.basis2_right}" name="basis2_right"
                                                  />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td width="100">Linkes Auge</td>
                                        <td><input type="text" value="{$rx.prisma1_left}" name="prisma1_left"/>
                                        </td>
                                        <td><input type="text" value="{$rx.basis1_left}" name="basis1_left"/>
                                        </td>
                                        <td><input type="text" value="{$rx.prisma2_left}" name="prisma2_left"/>
                                        </td>
                                        <td><input type="text" value="{$rx.basis2_left}" name="basis2_left"/>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table id="rx_table" class="table">
                                    <thead>

                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>HSA Brille</th>
                                        <th>HSA Rezept</th>
                                        <th>FSW</th>
                                        <th>Ink</th>
                                        <th>Kanal</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td width="100">Parameter</td>
                                        <td><input type="text" value="{$rx.mulitfocal_hsa}" name="mulitfocal_hsa"/></td>
                                        <td><input type="text" value="{$rx.mulitfocal_hsarx}" name="mulitfocal_hsarx"/></td>
                                        <td><input type="text" value="{$rx.mulitfocal_fsw}" name="mulitfocal_fsw"/></td>
                                        <td><input type="text" value="{$rx.mulitfocal_ink}" name="mulitfocal_ink"/></td>
                                        <td><input type="text" value="{$rx.mulitfocal_kanal}" name="mulitfocal_kanal"/>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>

                                <table id="rx_table" class="table">
                                    <tbody>
                                    <tr>
                                        <td>Status</td>
                                        <td>
                                            <select name="rx_valid">
                                                <option value="0" {if $rx.valid == 0}selected{/if}>Unbestätigt</option>
                                                <option value="1" {if $rx.valid == 1}selected{/if}>Ausgefüllt</option>
                                                <option value="2" {if $rx.valid == 2}selected{/if}>Bestätigt</option>
                                            </select>
                                        </td>

                                        <td><p><input type="submit" value="Rezept speichern"
                                                      class="btn btn-primary pull-right"/></p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </form>

                        <div class="col-xs-3 col-xs-offset-1">
                            <!-- Tab nav -->
                            <ul class="nav nav-tabs" id="optoTab">

                                <li class="{if isset($optovision[$rx.id_rx].glass)}active{/if}">
                                    <a href="#optovision-{$rx.id_rx}">
                                        Optovision <!-- <span class="badge">1</span> -->
                                    </a>
                                </li>
                                <li class="{if $optovision[$rx.id_rx].glass == NULL}active{/if}">
                                    <a href="#optiswiss-{$rx.id_rx}">
                                        OptiSwiss
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#essilor-{$rx.id_rx}">
                                        Essilor
                                    </a>
                                </li>
                            </ul>
                            <!-- Tab content -->
                            <div class="tab-content panel">
                                <!-- Tab optovision -->
                                <div class="tab-pane {if isset($optovision[$rx.id_rx].glass)}active{/if}"
                                     id="optovision-{$rx.id_rx}">
                                    {include file="$tpl_dir./tabs/optovision.tpl"}
                                </div>
                                <!-- Tab optiswiss -->
                                <div class="tab-pane {if $optovision[$rx.id_rx].glass == NULL}active{/if}"
                                     id="optiswiss-{$rx.id_rx}">
                                    {include file="$tpl_dir./tabs/optiswiss.tpl"}
                                </div>
                                <!-- Tab Essilor -->
                                <div class="tab-pane" id="essilor-{$rx.id_rx}">
                                    {include file="$tpl_dir./tabs/essilor.tpl"}
                                </div>
                            </div>

                            {if count($opti_log[$rx.id_order_detail]) >= 1}
                                <div class="rx-optiswiss-log">
                                    <p style="font-weight: bold">OptiSwiss Log</p>
                                    <div class="rx-optiswiss-wrapper">
                                        {foreach from=$opti_log[$rx.id_order_detail] item=log}
                                            <p>uploaded @ {$log.date} by {$log.firstname}</p>
                                        {/foreach}
                                    </div>
                                </div>
                            {/if}
                            {if count($optov_log[$rx.id_order_detail]) >= 1}
                                <div class="rx-optovision-log">
                                    <p style="font-weight: bold">Optovision Log</p>
                                    <div class="rx-optovision-wrapper">
                                        {foreach from=$optov_log[$rx.id_order_detail] item=log}
                                            <p>uploaded @ {$log.date} by {$log.firstname}</p>
                                        {/foreach}
                                    </div>
                                </div>
                            {/if}
                            {if count($essilor_log[$rx.id_order_detail]) >= 1}
                                <div class="rx-essilor-log">
                                    <p style="font-weight: bold">Essilor Log</p>
                                    <div class="rx-optovision-wrapper">
                                        {foreach from=$essilor_log[$rx.id_order_detail] item=log}
                                            <p>uploaded @ {$log.date} by {$log.firstname}</p>
                                        {/foreach}
                                    </div>
                                </div>
                            {/if}
                        </div>

                    </div>

                </div>
            </div>
        {/foreach}

    </div>

    <div class="row">
        <div class="col-xs-12">
            {if $rx.picture}
                <p><label>Foto</label></p>
                <div class="rx-img-wrapper">
                    <a href="{$rx.picture}" class="fancybox"><img src="{$rx.picture}" style="height: 200px;"/></a>
                    <p>&nbsp;</p>
                </div>
            {else}
                <p><label>Foto (neustes)</label></p>
                <div class="rx-img-wrapper">
                    {if $rx.latest_picture}
                        <a href="{$rx.latest_picture}" class="fancybox"><img src="{$rx.latest_picture}"
                                                                             style="height: 200px;"/></a>
                    {else}
                        <p>(kein)</p>
                        <p>&nbsp;</p>
                    {/if}
                </div>
            {/if}

        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseRXchange"
                    aria-expanded="false" aria-controls="collapseExample">
                Neues/Anderes Rezept auswählen
            </button>
        </div>
        <div class="col-sm-8 quittung-action">
            <form method="post" target="_blank">
                <input type="hidden" name="bo_viu_quittung" value="download"/>
                <input type="hidden" name="bo_viu_quittung_orderid" value="{$orderid}"/>
                <input type="text" name="bo_viu_quittung_amount" value="{$rx_amount}" style="width:100px;float:right"
                       class="rxs_amount"/>
                <input type="submit" value="Download Versicherungsquittung" class="btn btn-primary pull-right"/>
            </form>
            <form method="post">
                <input type="hidden" name="bo_viu_quittung" value="sendmail"/>
                <input type="hidden" name="bo_viu_quittung_orderid" value="{$orderid}"/>
                <input type="text" name="bo_viu_quittung_amount" value="{$rx_amount}" style="width:100px;float:right"
                       class="rxs_amount"/>
                <input type="submit" value="Maile Versicherungsquittung" class="btn btn-primary pull-right"/>
            </form>

        </div>
    </div>


    <div class="collapse" id="collapseRXchange" style="margin-top:30px;">
        <div class="well">
            <form method="post"><input type="hidden" name="rx_change_bo" value="reassign"/>
                <input type="hidden" name="rx_change_customer" value="{$rxidcustomer}"/>

                <p>Brille:</p>
                <p>
                    <select name="rx_change_idorderposition">
                        {foreach from=$orderProducts item=rx}
                            <option value="{$rx.id_order_detail}">{$rx.product_name} (Rezept: {$rx.id_rx})</option>
                        {/foreach}
                    </select>
                </p>
                <p>Rezept:</p>
                <p>
                    <select name="rx_change_rxid">
                        <option value="new">Neues Rezept</option>
                        <option value="no">Kein Rezept</option>
                        {foreach from=$rxsCustomer item=rxc}
                            <option value="{$rxc.id_rx}">Übernehme Werte aus dem Rezept vom {$rxc.date_add} | Status
                                = {$rxc.valid} | ID: {$rxc.id_rx}</option>
                        {/foreach}
                    </select>
                </p>

                <p><input type="submit" value="Änderung übernehmen" class="btn btn-primary"/></p>

            </form>
        </div>
    </div>


    <script>

        $('#optoTab a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')

        })

        $('#tabRxOrder a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });

        $(".rx-img-wrapper a").fancybox({
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'speedIn': 600,
            'speedOut': 200,
            'overlayShow': false
        });

        var $inputs = $(".rxs_amount");
        $inputs.keyup(function () {
            $inputs.val($(this).val());
        });

        $(document).ready(function () {
            $('#optovision-rxtype').val($('#rx_addition_type').val());
        });

    </script>
    <style>

        .quittung-action input {
            margin-right: 10px;
        }

        .rx-wrapper {
            width: 80%;
        }

        .rx-wrapper p {
            height: 40px;
        }

        .rx-wrapper p span {
            float: right;
        }

        .rx-optiswisslog-wrapper p {
            margin-bottom: 0;
            padding-bottom: 0;
        }

        input.rx-not-empty {
            background-color: #ffdce5 !important;
        }
    </style>

</div>
