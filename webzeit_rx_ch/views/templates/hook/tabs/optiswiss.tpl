<div class="form-horizontal">
    <div class="table-responsive">

        <form method="post"><input type="hidden" value="send"
                                   name="requestOptiSwiss"/> <input type="hidden"
                                                                    name="rx_id"
                                                                    value="{$rx.id_rx}"/><input
                    type="hidden" name="optiswiss_frame_name"
                    value="{$rx.product_name}"/>
            <p><label>OptiSwiss</label></p>

            {if $rx.valid == 2}
                {if $opti_frame[$rx.id_rx].code == NULL}
                    <p style="color: red; font-weight: bold">Produkt wurde noch
                        nicht konfiguriert!!!</p>
                {/if}
                <div class="rx-optiswiss-wrapper">
                    <table style="width: 100%">

                        <tr>
                            <td style="height: 40px;">Frame:</td>
                            <td>
                                {if $opti_frame[$rx.id_rx].code == NULL}
                                    {$opti_frame_list}
                                {else}
                                    {$opti_frame[$rx.id_rx]}
                                {/if}
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 40px;">Glas:</td>
                            <td>{$opti_glas}</td>
                        </tr>
                    </table>
                </div>
                <p>&nbsp;</p>
                <p><input type="submit" value="Upload To OptiSwiss Plattform"
                          class="btn btn-primary pull-right"/><br><br>
                {if $opti_sso[$rx.id_rx]}
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p><a href="{$opti_sso[$rx.id_rx]}" target="_blank"
                          class="btn btn-primary pull-right">Complete OptiSwiss
                            checkout</a></p>
                {/if}
            {else}
                <p style="color: red; font-weight: bold">Rezept nicht bestätigt.</p>
            {/if}


        </form>


    </div>
</div>