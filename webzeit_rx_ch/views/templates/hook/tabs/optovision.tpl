<div class="form-horizontal">
    <div class="table-responsive">
        <p><label>Optovision</label></p>
        {if $optovision[$rx.id_rx].glass == NULL}
        <p style="color: red; font-weight: bold">Momentan keine Gläser
            verfügbar!!!</p>
        {elseif $optovision[$rx.id_rx].frame == NULL}
        <p style="color: red; font-weight: bold">Produkt wurde noch nicht
            konfiguriert!!!</p>
        {else}
        <form method="post"
              onsubmit="return confirm('Gläser wirklich bei Optovision bestellen?');">
            <input type="hidden" value="send" name="requestOptovision"/>
            <input type="hidden" name="rx_id" value="{$rx.id_rx}"/>

            <table style="width: 100%">
                <tr style="height: 40px; width: 40%">
                    <td>Frame:</td>
                    <td>{$optovision[$rx.id_rx].frame}</td>
                </tr>
                <tr style="height: 40px">
                    <td>Glas:</td>
                    <td>
                        {foreach from=$optovision[$rx.id_rx].glass item=glass name=glassloop}
                            <input type="radio" name="optovision-glass"
                                   value="{$glass.code}"
                                   {if $smarty.foreach.glassloop.first}checked{/if}>
                            {$glass.title} ({$glass.code})
                            <br>
                        {/foreach}
                    </td>
                </tr>

                {if $optovision[$rx.id_rx].colors != NULL}
                    <tr style="height: 40px">
                        <td>Glasfarbe:</td>
                        <td>
                            <select name="optovision-glasscolor">
                                <option value="">-- keine Farbe --</option>
                                {foreach from=$optovision[$rx.id_rx].colors item=color}
                                    <option value="{$color.code}">{$color.desc}
                                        ({$color.code})
                                    </option>
                                {/foreach}
                            </select>
                        </td>
                    </tr>
                {/if}
                <tr style="height: 40px">
                    <td>Entsp.</td>
                    <td><input type="checkbox"
                               value="{$optovision[$rx.id_rx].antiref.code}"
                               name="optovision-antiref" checked>{$optovision[$rx.id_rx].antiref.title}
                        ({$optovision[$rx.id_rx].antiref.code})
                    </td>
                </tr>
                <tr style="height: 40px">
                    <td>Rezept-Typ</td>
                    <td><input type="text" name="optovision-rxtype" id="optovision-rxtype" disabled>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        {if $rx.valid == 2}
                            <p><input type="submit" value="Order by optovision"
                                      class="btn btn-primary pull-right"/></p>
                        {else}
                            <p>&nbsp;</p>
                            <p style="color: red; font-weight: bold">Rezept nicht
                                bestätigt.</p>
                        {/if}
                    </td>
                </tr>
            </table>
            {/if}
        </form>
    </div>
</div>