<div class="form-horizontal">
    <div class="table-responsive">

        <form method="post"><input type="hidden" value="send"
                                   name="requestEssilor"/> <input type="hidden"
                                                                    name="rx_id"
                                                                    value="{$rx.id_rx}"/><input
                    type="hidden" name="essilor_frame_name"
                    value="{$rx.product_name}"/>
            <p><label>Essilor</label></p>
            {if $rx.valid == 2}
                {if $essilor['frames'][$rx.id_rx].code == NULL}
                    <p style="color: red; font-weight: bold">Produkt wurde noch
                        nicht konfiguriert!!!</p>
                {/if}
                <div class="rx-essilor-wrapper">
                    <table style="width: 100%">

                        <tr>
                            <td style="height: 40px;">Frame:</td>
                            <td>
                                {if $essilor['frames'][$rx.id_rx].code == NULL}
                                    {$essilor['frames']['all']}
                                {else}
                                    {$essilor['frames'][$rx.id_rx]}
                                {/if}
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 40px;">Glas:</td>
                            <td>{$essilor['glasses']}</td>
                        </tr>
                        <tr style="height: 40px">
                            <td>Antiref.</td>
                            <td><input type="checkbox" value="089" name="antireflex" checked>HMC+(089)</td>
                        </tr>
                    </table>
                </div>
                <p>&nbsp;</p>
                <p><input type="submit" value="Upload To Essilor Plattform"
                          class="btn btn-primary pull-right"/><br><br>
                {if $essilor_sso[$rx.id_rx]}
                    <p><a href="{$essilor_sso[$rx.id_rx]}" target="_blank"
                          class="btn btn-primary pull-right">Complete Essilor
                            checkout</a></p>
                {/if}
            {else}
                <p style="color: red; font-weight: bold">Rezept nicht bestätigt.</p>
            {/if}


        </form>


    </div>
</div>