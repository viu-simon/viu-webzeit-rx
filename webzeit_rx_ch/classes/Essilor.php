<?php

/**
 * Created by PhpStorm.
 * User: simon
 * Date: 30.01.17
 * Time: 11:13
 */
class Essilor
{
    private $table_frame;
    private $table_glass;
    private $id_employee;

    public function __construct($id_employee)
    {
        $this->table_frame = 'ps_opt_essilor_frames';
        $this->table_glass = 'ps_opt_essilor_glasses';
        $this->table_log = 'ps_opt_essilor_log';
        $this->id_employee = $id_employee;
    }

    public function provideData($orderProducts)
    {
        $ret['frames'] = $this->provideFrames($orderProducts);
        $ret['frames']['all'] = '<select name="oswiss_frame">' . $this->allFrames() . '</select>';
        $ret['glasses'] = $selGlas = '<select name="oswiss_glas">' . $this->provideGlasses() . '</select>';

        return $ret;
    }

    public function provideFrames($orderProducts)
    {
        foreach ($orderProducts as $order) {
            $sql = 'SELECT frame FROM ps_viu_products WHERE id_product = ' . $order['product_id'];

            $frame = $this->getFrame(Db::getInstance()->getValue($sql));

            $selection = null;

            foreach ($frame as $k => $v) {
                $selection .= '<option value="' . $v['code'] . '">' . $v['product'] . '</option>';
            }
            $selFrame = '<select name="oswiss_frame">' . $selection . '</select>';

            if ($selection != NULL)
                $ret[$order['id_rx']] = $selFrame;
        }
        return $ret;
    }

    public function getFrame($frame)
    {
        $sql = "SELECT product, code FROM " . $this->table_frame . " WHERE frame  = '$frame'";
        return Db::getInstance()->ExecuteS($sql);
    }

    public function allFrames()
    {
        $ossFrames = Db::getInstance()->executeS('SELECT * FROM ' . $this->table_frame);
        foreach ($ossFrames as $k => $v)
            $selFrame .= '<option value="' . $v['code'] . '">' . $v['product'] . '</option>';

        return $selFrame;
    }

    public function provideGlasses()
    {
        $ossGlasses = Db::getInstance()->executeS('SELECT * FROM ' . $this->table_glass . ' ORDER BY position ASC ');
        foreach ($ossGlasses as $k => $v)
            $selGlas .= '<option value="' . $v['code'] . '">' . $v['name'] . ' (' . $v['description'] . ')</option>';
        return $selGlas;
    }

    public function sendData($id_order)
    {

        $id_rx = Tools::getValue('rx_id');

        if ($id_rx > 0)
            $res = $this->SOAPrequest($id_order, $id_rx);

        if (strlen($res->UploadCustomFileResult->Url) > 3) {

            // change order status
            $history = new OrderHistory();
            $history->id_order = (int)$id_order;
            $history->id_employee = (int)$this->context->employee->id;
            $history->id_order_state = 4;
            $history->changeIdOrderState(4, $id_order);
            $history->add();

            // return checkout url
            return array($id_rx => $res->UploadCustomFileResult->Url);
        }
    }

    private function SOAPrequest($id_order, $id_rx)
    {

        $order = new Order($id_order);
        $customer = new Customer($order->id_customer);
        $rx = webzeitVIUrx::getRXOrderDetailsByRX($id_order, $id_rx);

        $b2bxml = $this->createB2BxmlOrder($order, $customer, $rx);

        $requestParams = array(
            'username' => 'SWDE#8004170',
            'password' => 'Prevencia03',
            'refid' => 'VWVSHOP',
            'locale' => 'de-DE',
            'data' => utf8_encode($b2bxml),
        );

        $urlprod = 'https://services.opsysweb.eu/FileUpload.asmx?WSDL';
        $urlint = 'http://services.int.opsysweb.eu/FileUpload.asmx?WSDL';

        try {
            $opts = array(
                'ssl' => array('ciphers'=>'RC4-SHA', 'verify_peer'=>false, 'verify_peer_name'=>false)
            );
            $params = array ('encoding' => 'UTF-8', 'verifypeer' => false, 'verifyhost' => false, 'soap_version' => SOAP_1_2, 'trace' => 1, 'exceptions' => 1, "connection_timeout" => 180, 'stream_context' => stream_context_create($opts) );
            $client = new SoapClient($urlprod, $params);
            $response = $client->UploadCustomFile($requestParams);
        } catch (Exception $ex) {
            echo "fehler";
            die("soap error: " . $client->__getLastResponse());
        } finally {
            Db::getInstance()->execute('INSERT INTO ' . $this->table_log . ' (id_customer,id_order,id_rx,id_employee,date,response,product,id_order_detail, glass, frame) 
										VALUES(' . $order->id_customer . ',' . $id_order . ',' . $id_rx . ',' . $this->id_employee . ',"' . date('Y-m-d H:i:s') . '","' . $response->url . '","' . $rx['product_name'] . '","' . $rx['id_order_detail'] . '","' . Tools::getValue('oswiss_glas') . '","' . Tools::getValue('oswiss_frame') . '")');

            return $response;
        }
    }

    private function createB2BxmlOrder($order, $customer, $rx)
    {

        /*
        $products = $order->getProducts();
        foreach($products as $product)
            $model .= Db::getInstance()->getValue('SELECT name FROM ps_product_lang WHERE id_lang = 1 AND id_product ='.$product['product_id']);
        $model = trim($model, ' / ');
        */

        $modelparts = explode(" - ", Tools::getValue('optiswiss_frame_name'));
        $model = $modelparts[0];

        $presets = array('cid' => '999', 'cid' => '999-1',
            'glas' => Tools::getValue('oswiss_glas'),
            'frame' => Tools::getValue('oswiss_frame')
        );

        $default_height = Db::getInstance()->getRow('SELECT * FROM ps_optiswiss_frames WHERE code ="' . Tools::getValue('oswiss_frame') . '"');
        if ($rx['view_left'] == 0 || $rx['view_left'] == "0.00" || empty($rx['view_left']))
            $rx['view_left'] = $default_height['height'];
        if ($rx['view_right'] == 0 || $rx['view_right'] == "0.00" || empty($rx['view_right']))
            $rx['view_right'] = $default_height['height'];


        if (Tools::getValue('antireflex') != '') {
            $antireflex = '<coating coatingType="ANTIREFLEX">
                            <commercialCode>' . Tools::getValue('antireflex') . '</commercialCode>
                        </coating>';
        }

        $xml = '
			<b2bOptic
			    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://schema.b2boptic.com/b2boptic_v1.6.0.xsd">
			    <header msgType="ORDER">
			        <customersOrderId>' . $presets['cid'] . '</customersOrderId>
			        <distributorsOrderId>0</distributorsOrderId>
			        <timeStamps>
			            <dateTime step="CREATE">' . date('c') . '</dateTime>
			        </timeStamps>
			        <orderParties role="ORIGINATOR">
			            <id>' . $presets['coid'] . '</id>
			            <name>VIU VENTURES AG</name>
			            <address>
			                <addressLine>Weststrasse 70</addressLine>
			                <city>Zürich</city>
			                <zip>8003</zip>
			                <countryCode>CH</countryCode>
			            </address>
			        </orderParties>
					<software typeOf="SENDER">
			            <name>ShopBackOffice</name>
			            <version>1.5</version>
			        </software>
			        <productCatalog>
			            <name>sf6</name>
			            <release>0</release>
			        </productCatalog>
			        <portalOrderId>0</portalOrderId>
			    </header>
				<items>
			      <item>
			            <parties role="SHIPTO">
			                <id>' . $order->id_customer . '</id>
			            </parties>
			            <referenceNo>' . $order->id . '</referenceNo>
			            <referenceText>' . $order->id . '_' . substr($order->reference, 0, 3) . '_' . $customer->lastname . '_' . $model . '</referenceText>
			            <manufacturer></manufacturer>

			            <pair>
			                <patient>
			                    <id>' . $order->id_customer . '</id>
			                    <name>' . $customer->firstname . ' ' . $customer->lastname . '</name>
			                    <contact>
			                        <firstName>' . $customer->firstname . '</firstName>
			                        <lastName>' . $customer->lastname . '</lastName>
			                    </contact>
			                    <interpupillaryDistanceRight>' . $rx['pd_right'] . '</interpupillaryDistanceRight>
			                    <interpupillaryDistanceLeft>' . $rx['pd_left'] . '</interpupillaryDistanceLeft>
			                     
			                </patient>			                
			                <lens quantity="1" side="RIGHT">
			                    <commercialCode>' . $presets['glas'] . '</commercialCode>
			                    <rxData>
			                        <sphere>' . $rx['r_eye_spherical'] . '</sphere>
			                        <cylinder>
			                            <power>' . $rx['r_eye_cylindrical'] . '</power>
			                            <axis>' . $rx['r_eye_axis'] . '</axis>
			                        </cylinder>
			                        <addition>' . $rx['addition_right'] . '</addition>
			                    </rxData>

                        ' . $antireflex . '
                        
                        <coating coatingType="OTHER">
                            <commercialCode></commercialCode>
                        </coating>
                        
			                    <centration>
			                        <monocularCentrationDistance reference="FAR">' . $rx['pd_right'] . '</monocularCentrationDistance>
			                        <height reference="FAR" referenceHeight="OVERBOX">' . $rx['view_right'] . '</height>
			                         
			                    </centration>
			                    <geometry>
			                        <diameter>
			                            <physical>65</physical>
			                            <optical>70</optical>
			                        </diameter>
                                    <thickness reference="EDGE">2.00</thickness>
                                    <thicknessReduction reference="REDUCEWITHSHAPE">true</thicknessReduction>
			                    </geometry>
			                </lens>
			                <lens quantity="1" side="LEFT">
			                    <commercialCode>' . $presets['glas'] . '</commercialCode>
			                   <rxData>
			                        <sphere>' . $rx['l_eye_spherical'] . '</sphere>
			                        <cylinder>
			                            <power>' . $rx['l_eye_cylindrical'] . '</power>
			                            <axis>' . $rx['l_eye_axis'] . '</axis>
			                        </cylinder>
			                        <addition>' . $rx['addition_left'] . '</addition>
			                    </rxData>
			                    
                        ' . $antireflex . '
                        
                        <coating coatingType="OTHER">
                            <commercialCode></commercialCode>
                        </coating>
                    
			                    <centration>
			                        <monocularCentrationDistance reference="FAR">' . $rx['pd_left'] . '</monocularCentrationDistance>
			                        <height reference="FAR" referenceHeight="OVERBOX">' . $rx['view_left'] . '</height>
			                     
			                    </centration>
			                    <geometry>
			                        <diameter>
			                            <physical>65</physical>
			                            <optical>70</optical>
			                        </diameter>
                                    <thickness reference="EDGE">2.00</thickness>
                                    <thicknessReduction reference="REDUCEWITHSHAPE">true</thicknessReduction>
			                    </geometry>
			                </lens>
							
                            <frame quantity="1">
                                <material>PLASTIC</material>
                                <shape>
                                    <tracerData>
                                        <tracerType>Essilor TESS</tracerType>
                                        <tracerVersion>?</tracerVersion>
                                        <binaries format="OMA">' . $presets['frame'] . '</binaries>
                                    </tracerData>
                                </shape>
                                <boxWidth>53.90</boxWidth>
                                <boxHeight>34.80</boxHeight>
                                <distanceBetweenLenses>17.00</distanceBetweenLenses>
                            </frame>
							
                            <edging edgingType="ONSHAPE">
                                <bevel>
                                    <type>BEVEL</type>
                                    <position side="LEFT" posType="RELATION">0.4</position>
                                    <position side="RIGHT" posType="RELATION">0.4</position>
                                </bevel>
                                <polish>false</polish>
                                <chamfer position="BOTH">THIN</chamfer>
                            </edging>
							
			            </pair>
			            <options>
			                <insurance>true</insurance>
			            </options>
			        </item> 
			    </items>
			</b2bOptic>';

        $doc = new DOMDocument();
        $doc->loadXML($xml);
        $xmlstring = $doc->saveXML();
        //echo '<pre>', htmlentities($xmlstring), '</pre>';die();
        return $xmlstring;

    }

    public function is_valid_xml($xml)
    {
        libxml_use_internal_errors(true);
        $doc = new DOMDocument('1.0', 'utf-8');
        $doc->loadXML($xml);
        $errors = libxml_get_errors();
        return empty($errors);
    }

    public function getLog($id_order)
    {
        $sql = "SELECT ol.*, e.firstname FROM ps_opt_essilor_log ol
												 LEFT JOIN ps_employee e ON (e.id_employee = ol.id_employee) 
												 WHERE ol.id_order = $id_order ORDER BY ol.date";

        $query = Db::getInstance()->executeS($sql);

        foreach ($query as $log)
            $ret[$log['id_order_detail']][] = $log;

        return $ret;
    }
}