<?php


class webzeitVIUrx extends ObjectModel
{

	/** @var integer id*/
	public $id;

	public $id_order;
	public $id_cart;
	public $id_viu_cart;
	public $id_customer;
	public $prescription;
	public $r_eye_spherical;
	public $r_eye_cylindrical;
	public $r_eye_axis;
	public $l_eye_spherical;
	public $l_eye_cylindrical;
	public $l_eye_axis;
	public $pd_total;
	public $pd_left;
	public $pd_right;
	public $prescription_reading;
	public $addition_left;
	public $addition_right;
	public $view_left;
	public $view_right;
	public $picture;
	public $valid;
	public $valid_date;
	public $valid_employee;
	public $addition_type;
	
	
	public $prisma1_left;
	public $prisma2_left;
	public $basis1_left;
	public $basis2_left;
	
	public $prisma1_right;
	public $prisma2_right;
	public $basis1_right;
	public $basis2_right;
	
	public $mulitfocal_hsarx;
	public $mulitfocal_hsa;
	public $mulitfocal_fsw;
	public $mulitfocal_ink;
	public $mulitfocal_kanal;
	
	
	
	
	/** @var string Object creation date */
	public $date_add;

	/** @var string Object last modification date */
	public $date_upd;
	
	
	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'webzeit_rx',
		'primary' => 'id_rx',
		'multilang' => false,
		'fields' => array(
			'id_order' => 					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'id_cart' => 					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'id_viu_cart' => 					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'id_customer' =>				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'prescription' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'r_eye_spherical' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'r_eye_cylindrical' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'r_eye_axis' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'l_eye_spherical' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'l_eye_cylindrical' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'l_eye_axis' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'pd_total' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'pd_left' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'pd_right' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'prescription_reading' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),	
			'addition_type' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),	
			'addition_left' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'addition_right' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'view_left' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'view_right' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'picture' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'date_add' => 					array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
			'date_upd' => 					array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
			//'valid_date' => 					array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'lang' => false,),
			'valid' => 					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId','copy_post' => false ),
			'valid_employee' => 					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			
			'prisma1_left' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'prisma2_left' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'basis1_left' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'basis2_left' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'prisma1_right' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'prisma2_right' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'basis1_right' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),
			'basis2_right' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),	
			
			'mulitfocal_hsarx' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),	
			'mulitfocal_hsa' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),	
			'mulitfocal_fsw' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),	
			'mulitfocal_ink' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),	
			'mulitfocal_kanal' =>				array('type' => self::TYPE_STRING, 'lang' => false, 'validate' => 'isString'),	
			
			
			
			
		)
	);
	
	public function save($null_values = false, $autodate = true)
	{
	
		if($this->valid < 2)
			if(($this->r_eye_spherical != '' ||
			    $this->r_eye_cylindrical != '' ||
		        $this->r_eye_axis != '' ||	
			    $this->l_eye_spherical != '' ||
			    $this->l_eye_cylindrical != '' ||
		        $this->l_eye_axis != '' || strlen($this->picture) > 3)
		      )
		       	$this->valid = 1;
		      else
		      	$this->valid = 0; 
	    		 
		$return = parent::save($null_values, $autodate);
		return $return;
	}
	
	
	public static function getRXByCustomer($id_customer, $id_cart =  0) {
		
		if($id_cart > 0)
			return Db::getInstance()->executeS("SELECT * FROM ps_webzeit_rx WHERE id_customer = '".$id_customer."' AND id_cart != ".$id_cart." ORDER BY date_add DESC");
		else
			return Db::getInstance()->executeS("SELECT x.*, (SELECT y.picture FROM ps_webzeit_rx y WHERE y.id_customer = '".$id_customer."'  AND y.picture <> ''  ORDER BY y.date_add DESC LIMIT 1) as latest_picture FROM ps_webzeit_rx x WHERE x.id_customer = '".$id_customer."' ORDER BY x.date_add DESC");
	}
	public static function getImagesByCartId($id_cart,$id_customer) {
		return Db::getInstance()->executeS("SELECT id_rx, picture, id_viu_cart, id_cart, id_customer, date_add, date_upd FROM ps_webzeit_rx WHERE id_cart = '".$id_cart."' AND id_customer = '".$id_customer."'");
	}
	
	public static function getPK($id_cart,$id_customer,$id_viu_cart) {
		return Db::getInstance()->getValue("SELECT id_rx FROM ps_webzeit_rx WHERE id_cart = '".$id_cart."' AND id_customer = '".$id_customer."' AND id_viu_cart = '".$id_viu_cart."' ");
	}
	public static function getPath($id_cart,$id_rx) {
		return Db::getInstance()->getValue("SELECT picture FROM ps_webzeit_rx WHERE id_cart = '".$id_cart."' AND id_rx = '".$id_rx."'");
	}
	
	public static function setOrderId($id_cart,$id_customer,$id_order) {
		return Db::getInstance()->execute("UPDATE  ps_webzeit_rx SET id_order = '".$id_order."' WHERE id_cart = '".$id_cart."' AND id_customer = '".$id_customer."'");
	}
	
	public static function getRXOrderDetails($id_order) {
		return Db::getInstance()->executeS("SELECT od.id_order_detail, od.id_rx, od.product_name, rx.* FROM  ps_order_detail od
												LEFT JOIN ps_webzeit_rx as rx ON (rx.id_rx = od.id_rx)
													WHERE od.id_order = '".$id_order."' AND od.linkedto = '0' AND od.id_rx != 0");
	}
	public static function getRXOrderDetailsByRX($id_order, $id_rx) {
		return Db::getInstance()->getRow("SELECT od.id_order_detail, od.id_rx, od.product_name, rx.* FROM  ps_order_detail od
												LEFT JOIN ps_webzeit_rx as rx ON (rx.id_rx = od.id_rx)
													WHERE od.id_order = '".$id_order."' AND od.id_rx = '".$id_rx."' AND od.linkedto = '0'");
	}
	
	public function createNewFromOld($oldid) {
		
			$new = new webzeitVIUrx();
			$old = new webzeitVIUrx($oldid);
			
			$new->picture = $old->picture;
			$new->id_customer = $old->id_customer;
			$new->valid = 0;
			$new->r_eye_spherical = $old->r_eye_spherical;
			$new->r_eye_cylindrical = $old->r_eye_cylindrical;
			$new->r_eye_axis = $old->r_eye_axis;
			$new->l_eye_spherical = $old->l_eye_spherical;
			$new->l_eye_cylindrical = $old->l_eye_cylindrical;
			$new->l_eye_axis = $old->l_eye_axis;
			$new->pd_total = $old->pd_total;
			$new->pd_left = $old->pd_left;
			$new->pd_right = $old->pd_right;
			$new->addition_left = $old->addition_left;
			$new->addition_right = $old->addition_right;
						
			$new->save();
			
			return $new->id;
		
	}

	public function copyFromPost()
	{
		/* Classical fields */
		foreach ($_POST AS $key => $value)
			if (key_exists($key, $this) AND $key != 'id_rx')
				$this->{$key} = $value;

	}
	
}